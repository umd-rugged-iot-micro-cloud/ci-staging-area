node {
    def app

    stage('Clone repository') {
        /* Let's make sure we have the repository cloned to our workspace */

        checkout scm
    }

    stage('Build image') {
        /* This builds the actual image; synonymous to
         * docker build on the command line */
        dir("hello-world"){
            app = docker.build("rimcproject/hello-world")
        }
    }

    stage('Test image') {
        /* Ideally, we would run a test framework against our image.
         * For this example, we're using a Volkswagen-type approach ;-) */

        app.inside {
            sh 'echo "Tests passed"'
        }
    }

    stage('Push image') {
        /* Finally, we'll push the image with two tags:
         * First, the incremental build number from Jenkins
         * Second, the 'latest' tag.
         * Pushing multiple tags is cheap, as all the layers are reused. */
        docker.withRegistry('https://registry.hub.docker.com', 'docker-hub-credentials') {
            app.push("${env.BUILD_NUMBER}")
            app.push("latest")
        }
    }
    
    stage('Update kubernetes manifest') {
        // not proud of how this looks (like shit) but it does work fine
        // first remove any old folders 
        sh 'rm -rf k3s-deployments'
        // now clone k3s deployments into new folder
        sh 'git clone https://aleonti@bitbucket.org/umd-rugged-iot-micro-cloud/k3s-deployments.git k3s-deployments/'
        // navigate into the repo
        dir('k3s-deployments/hello-world') {
            // run a command to update the image version number
            sh '''
                newimg="docker.io/rimcproject/hello-world:$BUILD_NUMBER" \
                    yq e -i '.spec.containers[0].image = env(newimg)' hello_world.yaml
            '''
        }
        // navigate to repo top-level folder
        dir('k3s-deployments') {
            // commit and push changes
            sh '''
                git remote set-url --push origin https://aleonti:qFW3s4rmvQYjx2WNXbkD@bitbucket.org/umd-rugged-iot-micro-cloud/k3s-deployments.git
                git commit -am 'automated commit'
                git push
            '''
        }
    }
}
